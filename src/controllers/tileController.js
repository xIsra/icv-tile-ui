app.controller('tileController', [
  '$scope',
  '$rootElement',
  function($scope, $rootElement) {
    $scope.init = function(id) {
      $scope.id = id + 1;
      $scope.setLabel($scope.id);
      $scope.position = $scope.pathFinder.getPositionById($scope.id);
      $scope.tileColor = $scope.tile.defaultColor;
      $scope.mazeRun.addTile($scope);
    };

    /**
     * 0 = swap_color / default
     * 1 = start, first line tile
     * 2 = end, last line tile
     * 3 = block, blocking the path of the mazeRun
     * 4 = path, from start to end
     */
    $scope.state = 0;

    // $scope.id = num;
    // $scope.tileColor = $scope.tile.defaultColor;

    $scope.setLabel = function(param = null) {
      if (param == null) {
        $scope.label = $scope.id;
      }
      else{
        $scope.label = param;
      }
    };

    $scope.setDefaultTile = function(_scope) {
      _scope.state = 0;
      _scope.setLabel();
      _scope.tileColor = $scope.tile.defaultColor;
    };

    $scope.tileStyle = {
      // 'background-color': $scope.tile.defaultColor,
      // 'height': $scope.tileHeightDimeter - 2 + "px",
      // 'width': $scope.tileWidthDimeter - 2 + "px",
      // 'float': 'left',
      // 'border': '1px solid #111',
    };

    // $scope.tileAction = function() {
    //   return this.tileActions[$scope.scenario];
    // };

    $scope.tileActions = {
      'color_swap': function() {
        if ($scope.state !== 0) {
          return false;
        }
        if ($scope.tileColor === $scope.tile.firstColor) {
          $scope.tileColor = $scope.tile.defaultColor;
        } else {
          $scope.tileColor = $scope.tile.firstColor;
        }
      },
      'start': function() {
        if ($scope.state == 1 || $scope.state == 2) {
          return false;
        }
        if ($scope.mazeRun.startingTile !== null) {
          $scope.setDefaultTile($scope.mazeRun.startingTile);
        }
          $scope.scenarioSet();
          $scope.state = 1;
          $scope.mazeRun.startingTile = $scope;
          $scope.label = 'Start';
          $scope.tileColor = $scope.tile.startingTile;
          // $scope.switchColor($scope.tile.startingTile);
      },
      'end': function() {
        if ($scope.state == 1 || $scope.state == 2) {
          return false;
        }
        if ($scope.mazeRun.endingTile !== null) {
          $scope.setDefaultTile($scope.mazeRun.endingTile);
        }

        $scope.scenarioSet();
        $scope.state = 2;

        $scope.mazeRun.endingTile = $scope;
        $scope.label = 'End';
        $scope.tileColor = $scope.tile.endingTile;

      },
      'block': function() {
        if ($scope.state == 3) {
          delete $scope.mazeRun.blockingTiles[$scope.id];
          $scope.setDefaultTile($scope);
          return false;
        }
        if ($scope.state !== 0) {
          return false;
        }
        $scope.state = 3;
        $scope.label = 'BLOCK';
        $scope.tileColor = $scope.tile.blockingTile;

        $scope.mazeRun.blockingTiles[$scope.id] = $scope;
      }
    };

  }
]);

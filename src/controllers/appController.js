app.controller('appController', [
  '$scope',
  '$window',
  function($scope, $window) {

    $scope.config = {
      name:"Maze Run",
      author:"xIsra"

    };
    $scope.helpers = {
      isPositionEqual: function(one, two) {
        if (one[0] === two[0] && one[1] === two[1]) {
          return true;
        }
        return false;
      }
    };

  }
]);

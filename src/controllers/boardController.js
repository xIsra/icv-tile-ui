app.controller('boardController', [
  '$scope',
  '$window',
  '$timeout',
  function($scope, $window, $timeout) {
    /**
     * scenario = global state of tile actions
     * color_swap = change color to tile.firstColor model
     * start = writing the word start on the tile, state = 0
     */
    $scope.scenario = 'color_swap';
    $scope.scenarioSet = function(params = null) {
      if (params === null) {
        $scope.scenario = 'color_swap';
      } else {
        $scope.scenario = params;
      }
    }

    $scope.x_count = 12;
    $scope.y_count = 12;

    /**
     * mazeRun: object containing all the stage and operations
     */
    $scope.mazeRun = {
      x_count: $scope.x_count,
      y_count: $scope.y_count,
      total_count: this.x_count * this.y_count,

      tiles: [],
      startingTile: null,
      endingTile: null,
      blockingTiles: {},

      addTile: function(scope) {
        this.tiles[scope.id] = scope;
      },
      getTileById: function(id) {
        return this.tiles[id];
      },
      getTileByPosition: function(position) {
        for (var i = 1; i <= this.tiles.length; i++) {
          var _t = this.getTileById(i);
          if ($scope.helpers.isPositionEqual(_t.position, position)) {
            return _t;
          }
        }
        return false;
      },
    };
    /**
     * pathFinder: object of methods to calculate path
     * tile.position = [int column, int row]
     * return array [[x,x],[x,x]]
     */
    $scope.pathFinder = {
      // path: array contains integers of tile ID
      // path: {},
      // showPath: function displaying path by Interval and Style
      showPath: function(delay = 500) {
        this.clearPath();
        var pathArray = this.findPath();

        if (pathArray == false) {
          return false;
        }

        for (var i = 0; i < pathArray.length; i++) {
          var currentTile = $scope.mazeRun.getTileByPosition(pathArray[i]);
          if (currentTile !== false) {
            currentTile.state = 4;
            currentTile.tileColor = $scope.tile.pathColor;
          }
        }

      },
      // findPath: function calculating the tiles which are clossest
      findPath: function() {
        if ($scope.mazeRun.startingTile == null || $scope.mazeRun.endingTile == null) {
          // console.log("No startingTile or endingTile");
          return false;
        }
        var startingTile = $scope.mazeRun.startingTile;
        var endingTile = $scope.mazeRun.endingTile;
        var pathArray = [];

        // direct path loop
        var looper = function(currentPosition) {
          var nextPosition = $scope.pathFinder.getNextPosition(
            currentPosition,
            endingTile.position
          );
          if (!nextPosition) {
            return false;
          }
          console.log("PATH:" + nextPosition);
          pathArray.push(nextPosition);
          looper(nextPosition);
        };
        looper(startingTile.position);

        return pathArray;
      },
      clearPath: function() {
        for (var i = 1; i < $scope.mazeRun.tiles.length; i++) {
          var co = $scope.mazeRun.getTileById(i);
          if (co.state == 4) {
            co.setDefaultTile(co);
          }
        }
        return false;
      },
      /**
       * getNextPosition: Movement logic
       */
      getNextPosition: function(currentPosition, endPosition) {
        var nextPosition = [];

        if (currentPosition[0] < endPosition[0]) {
          nextPosition[0] = currentPosition[0] + 1;
        } else if (currentPosition[0] > endPosition[0]) {
          nextPosition[0] = currentPosition[0] - 1;
        } else {
          nextPosition[0] = currentPosition[0];
        }

        if (currentPosition[1] < endPosition[1]) {
          nextPosition[1] = currentPosition[1] + 1;
        } else if (currentPosition[1] > endPosition[1]) {
          nextPosition[1] = currentPosition[1] - 1;
        } else {
          nextPosition[1] = currentPosition[1];
        }

        var nextTile = $scope.mazeRun.getTileByPosition(nextPosition);
        if (nextTile.state > 0) {
          // return this.getNextPosition(nextPosition,endPosition);
        }


        if ($scope.helpers.isPositionEqual(nextPosition, endPosition)) {
          return false;
        }

        return nextPosition;
      },
      getPositionById: function(id) {
        return [this.getCloumn(id), this.getRow(id)];
      },
      getCloumn: function(id) {
        var tav = $scope.mazeRun.x_count * (this.getRow(id) - 1);
        return id - tav;
      },
      getRow: function(id) {
        return Math.ceil(id / $scope.mazeRun.x_count);
      },
    };

    $scope.tile = {
      defaultColor: "#bdc3c7",
      firstColor: "#2ecc71",
      pathColor: "#f39c12",
      startingTile: "#f1c40f",
      endingTile: "#c0392b",
      blockingTile: "#2c3e50",
    };

    $scope.containerDimeter = $window.innerWidth * 0.5;
    $scope.tileWidthDimeter = $scope.containerDimeter / $scope.x_count;
    $scope.tileHeightDimeter = $scope.containerDimeter / $scope.y_count;

    $scope.clearAllTiles = function() {
      for (var i = 1; i < $scope.mazeRun.tiles.length; i++) {
        var co = $scope.mazeRun.getTileById(i);
        co.setDefaultTile(co);
      }
      return false;
    };




    $scope.getTiles = function(total) {
      var tiles = [];
      for (var i = 0; i < total; i++) {
        tiles.push(i);
      };
      return tiles;
    };
  }
]);
